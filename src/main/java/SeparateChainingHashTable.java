/**
 * K <- data type for the key
 *      - no primitives
 *      - must implement hashCode() and equals() methods
 * V <- data type for the value
 *      - no primitives
 *      - must implement hashCode() and equals() methods
 */

import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;

public class SeparateChainingHashTable<K, V> {
    private static final int DEFAULT_ARRAY_SIZE = 3;
    private static final double LOWEST_LOAD_FACTOR = 0.30;
    private static final double LARGEST_LOAD_FACTOR = 0.75;
    private static final int ARRAY_SIZE_MODIFIER = 2;

    private int arraySize;
    private List<Entry<K, V>>[] array; //array of linked lists containing entries
    private int countOfEntries;

    public SeparateChainingHashTable() {
        arraySize = DEFAULT_ARRAY_SIZE;
        System.out.println("Default array size: " + DEFAULT_ARRAY_SIZE + "\n");
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
    }

    public V get(K key) {
        Entry<K, V> entry = getEntry(key);

        if (entry == null) {
            return null;
        } else {
            return entry.value;
        }
    }

    public void put(K key, V value) {
        int index = getIndex(key);
        List<Entry<K, V>> entries = array[index];

        if (entries == null) { //if there is no value with this hash
            entries = new LinkedList<>();
            Entry entry = new Entry();
            entry.key = key;
            entry.value = value;
            entries.add(entry);
            countOfEntries++;
            array[index] = entries;

            if (calculateLoadFactor() > LARGEST_LOAD_FACTOR) {
                increaseArray();
            }
        } else { //there are any values with this hash
            for (Entry e : entries) {
                if (e.key.equals(key)) { //if there is a entry with this key, update the value
                    e.value = value;
                    return;
                }
            }

            Entry<K, V> entry = new Entry<>(); //there is not any entry with this key so, create new entry and add it
            entry.key = key;
            entry.value = value;
            entries.add(entry);
            countOfEntries++;

            if (calculateLoadFactor() > LARGEST_LOAD_FACTOR) {
                increaseArray();
            }
        }
    }

    public boolean delete(K key) {
        int index = getIndex(key);
        List<Entry<K, V>> entries = array[index];

        if (entries == null) {
            return false; //there is no list containing such a key so nothing is deleted
        } else {
            for (Entry e : entries) {
                if (e.key.equals(key)) {
                    entries.remove(e); //delete entry with this key
                    countOfEntries--;

                    if (array[index].size() == 0) {
                        array[index] = null;
                    }

                    if (calculateLoadFactor() < LOWEST_LOAD_FACTOR) {
                        decreaseArray();
                    }
                    return true;
                }
            }
            return false; //list does not contain this key so nothing is deleted
        }
    }

    private int getIndex(K key) { //calculates the array index for a given hash
        return (key.hashCode() & 0x7fffffff) % arraySize; // returns the array index which is the bucket for the list
    }

    private Entry<K, V> getEntry(K key) {
        if (key == null) { //invalid key
            return null;
        } else { //valid key
            int index = getIndex(key);
            List<Entry<K, V>> entries = array[index];
            if (entries == null) { // if entry for this key does not exist
                return null;
            } else { //entry exists so, find it
                for (Entry e : entries) {
                    if (e.key.equals(key)) {
                        return e;
                    }
                }
                return null;
            }
        }
    }

    private List<Entry<K, V>>[] createArrayWithNullValues(int size) {
        List<Entry<K, V>>[] newArray = new LinkedList[size];

        Arrays.fill(newArray, null);
        return newArray;
    }

    private double calculateLoadFactor() {
        System.out.println("Count of entries: " + countOfEntries + " | " + "Array size: " + arraySize + " | " + "Load factor: " + (1.0 * countOfEntries) / arraySize + " ");
        return (1.0 * countOfEntries) / arraySize;
    }

    private void decreaseArray() {
        List<Entry<K, V>>[] buckets = array;
        arraySize /= ARRAY_SIZE_MODIFIER;
        System.out.println("Decrease array size by " + ARRAY_SIZE_MODIFIER);
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
        rehash(buckets);
    }

    private void increaseArray() {
        List<Entry<K, V>>[] buckets = array;
        arraySize *= ARRAY_SIZE_MODIFIER;
        System.out.println("Increase array size by " + ARRAY_SIZE_MODIFIER);
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
        rehash(buckets);
    }

    private void rehash(List<Entry<K, V>>[] buckets) {
        for (int i=0; i<buckets.length; i++) {
            if (buckets[i] == null) {
                continue;
            } else {
                buckets[i].forEach(entry -> {
                    put(entry.key, entry.value);
                });
            }
        }
    }

    @Override
    public String toString() {
        return "SeparateChainingHashTable{" +
                "array=" + Arrays.toString(array) +
                '}';
    }
}
