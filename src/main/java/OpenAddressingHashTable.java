import java.util.Arrays;

public class OpenAddressingHashTable<K, V> {
    private static final int DEFAULT_ARRAY_SIZE = 3;
    private static final double LOWEST_LOAD_FACTOR = 0.30;
    private static final double LARGEST_LOAD_FACTOR = 0.75;
    private static final int ARRAY_SIZE_MODIFIER = 2;

    private int arraySize;
    private Entry<K, V>[] array; //array of linked lists containing entries
    private int countOfEntries;

    public OpenAddressingHashTable() {
        arraySize = DEFAULT_ARRAY_SIZE;
        System.out.println("Default array size: " + DEFAULT_ARRAY_SIZE);
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
    }

    public V get(K key) {
        Entry<K, V> entry = getEntry(key);

        if (entry == null) {
            return null;
        } else {
            return entry.value;
        }
    }

    public void put(K key, V value) {
        int index = getIndex(key);
        putIntoFirstEmptyBucket(index, key, value);
    }

    public boolean delete(K key) {
        int index = getIndex(key);
        return deleteEntry(index, key);
    }

    private Entry<K, V>[] createArrayWithNullValues(int size) {
        Entry<K, V>[] newArray = new Entry[size];

        Arrays.fill(newArray, null);
        return newArray;
    }

    private Entry<K, V> getEntry(K key) {
        if (key == null) { //invalid key
            return null;
        } else { //valid key
            int index = getIndex(key);
            return findEntry(index, key);
        }
    }

    private Entry<K, V> findEntry(int index, K key) {
        Entry<K, V> entry = array[index];
        if (entry == null) { // if entry for this key does not exist
            return null;
        } else { //entry exists so, find it
            if (entry.key.equals(key)) {
                return entry;
            } else {
                if (index == arraySize -1) {
                    return findEntry(0, key); // next key, TO-DO : implement double hashing
                } else {
                    return findEntry(index + 1, key); // next key, TO-DO : implement double hashing
                }
            }
        }
    }

    private double calculateLoadFactor() {
        System.out.println("Count of entries: " + countOfEntries + " | " + "Array size: " + arraySize + " | " + "Load factor: " + (1.0 * countOfEntries) / arraySize + " ");
        return (1.0 * countOfEntries) / arraySize;
    }

    private void decreaseArray() {
        Entry<K, V>[] bucket = array;
        arraySize /= ARRAY_SIZE_MODIFIER;
        System.out.println("Decrease array size by " + ARRAY_SIZE_MODIFIER);
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
        rehash(bucket);
    }

    private void increaseArray() {
        Entry<K, V>[] buckets = array;
        arraySize *= ARRAY_SIZE_MODIFIER;
        System.out.println("Increase array size by " + ARRAY_SIZE_MODIFIER);
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
        rehash(buckets);
    }

    private void rehash(Entry<K, V>[] buckets) {
        for (int i=0; i<buckets.length; i++) {
            if (buckets[i] == null) {
                continue;
            } else {
                put(buckets[i].key, buckets[i].value);
            }
        }
    }

    private void rehashArray() {
        Entry<K, V>[] buckets = array;
        System.out.println("Rehash array after entry deleted");
        array = createArrayWithNullValues(arraySize);
        countOfEntries = 0;
        rehash(buckets);
    }

    private void putIntoFirstEmptyBucket(int index, K key, V value) {
        Entry<K, V> entry = array[index];

        if (entry == null) { // or is marked as deleted    //if there is no value with this hash
            entry = new Entry<>();
            entry.key = key;
            entry.value = value;
            array[index] = entry;
            countOfEntries++;

            if (calculateLoadFactor() > LARGEST_LOAD_FACTOR) {
                increaseArray();
            }
        } else { //there are any values with this hash
            if (entry.key.equals(key)) { //if there is a entry with this key, update the value
                entry.value = value;
                return;
            } else {
                if (index == arraySize-1) {
                    putIntoFirstEmptyBucket(0, key, value);
                } else {
                    putIntoFirstEmptyBucket(index + 1, key, value);
                }
            }
        }
    }

    private boolean deleteEntry(int index, K key) {
        Entry<K, V> entry = array[index];
        if (entry == null) {
            return false; //there is no entry containing such a key so nothing is deleted
        } else {
            if (entry.key.equals(key)) {
                array[index] = null; //delete entry with this key     //mark as deleted
                countOfEntries--;

                if (calculateLoadFactor() < LOWEST_LOAD_FACTOR) {
                    decreaseArray(); //decrease array size and rehash
                } else {
                    rehashArray(); //just rehash (because after delete, find method does not work properly), array size without change
                }
                return true;
            } else {
                if (index == arraySize - 1) {
                    return deleteEntry(0, key);
                } else {
                    return deleteEntry(index + 1, key);
                }
            }
        }
    }

    private int getIndex(K key) { //calculates the array index for a given hash
        return (key.hashCode() & 0x7fffffff) % arraySize; // returns the array index which is the bucket for the list
    }

    @Override
    public String toString() {
        return "OpenAddressingHashTable{" +
                ", array=" + Arrays.toString(array) +
                '}';
    }
}
