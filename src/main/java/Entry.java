public class Entry<K, V> {
    public K key;
    public V value;

    @Override
    public String toString() {
        return "Entry{" +
                "key=" + key +
                ", value=" + value +
                '}';
    }
}